# Lions MD410 2023 Oceans of Opportunity Convention Registration Form Server

A [FastAPI](https://fastapi.tiangolo.com/) server to receive registration form data from the [Netlify](https://www.netlify.com/)-hosted [Lions MD410 2023 Convention website](https://lionsconvention.co.za/), extract the useful fields into a JSON dictionary and push that dictionary into an [AWS SQS queue](https://aws.amazon.com/sqs/) for other tooling to consume.

# Operation

The server is most easily used as a Docker image: registry.gitlab.com/md410-2023-convention/reg-form-server. It requires environment variables for writing to SQS:

* AWS_ACCESS_KEY_ID
* AWS_SECRET_ACCESS_KEY

Optionally, the **DEBUG** environment variable can be set to toggle debug output.
